﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Controllers
{
    internal class AccessDeviceController
    {
        private readonly IAccessManager _accessManager;
        private readonly IIOTDeviceController _deviceController;

        private List<AccessDevice> accessDevices = new List<AccessDevice>();

        public AccessDeviceController(IAccessManager accessManager, IIOTDeviceController deviceController)
        {
            _accessManager = accessManager;
            _deviceController = deviceController;
        }

        public void LoadDevices()
        {
            foreach (var device in ConfigurationManager.AppSettings.Get("AccessDevices").Split(", "))
            {
                accessDevices.Add(new AccessDevice(_accessManager, _deviceController, device));
            }
            Console.WriteLine("Loaded {0} Access Controllers", accessDevices.Count);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Controllers
{
    internal class IOTDevice
    {
        private SerialPort serialPort;

        public string Port { get; set; }

        public IOTDevice(string port)
        {
            Port = port;
            serialPort = new SerialPort(port, 9600);
            serialPort.DataReceived += SerialPort_DataReceived;
            serialPort.Open();
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Console.WriteLine(serialPort.ReadLine());
        }

        public void ControlDevice(int id, bool state)
        {
            serialPort.WriteLine(string.Format("toggle {0} {1}", id, state ? "true" : "false"));
            Console.WriteLine("Ref: " + string.Format("toggle {0} {1}", id, state ? "true" : "false"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Controllers
{
    internal class TestController
    {
        private IAccessManager _accessManager;

        public TestController(IAccessManager accessManager)
        {
            _accessManager = accessManager;
            WaitForCheckIn();
        }

        public void WaitForCheckIn()
        {
            while (true)
            {
                var code = Console.ReadLine();

                var result = _accessManager.CheckInOrOut(code).Result;
                if(result != null)
                    Console.WriteLine("Hello " + result.Employee.FullName);
                else
                    Console.WriteLine("Access denied");
            }
        }
    }
}

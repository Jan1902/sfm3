﻿using SFM3.Shared.Models.AccessControl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Controllers
{
    internal class AccessDevice
    {
        private SerialPort serialPort;
        private IAccessManager _accessManager;
        private IIOTDeviceController _iotDeviceController;

        public AccessDevice(IAccessManager accessManager, IIOTDeviceController iotDeviceController, string port)
        {
            _accessManager = accessManager;
            _iotDeviceController = iotDeviceController;

            serialPort = new SerialPort(port, 9600);

            serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            serialPort.Open();
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var request = serialPort.ReadLine();
            Console.WriteLine("Incoming Request on Serial Port '{0}'", serialPort.PortName);

            var parts = request.Split(' ');

            if (parts.Length != 2)
                return;

            if (parts[0] == "checkIn")
            {
                var code = parts[1].Replace("\r", "");

                var result = _accessManager.CheckInOrOut(code).Result;

                if (result != null)
                {
                    var devices = _accessManager.GetDevicesToToggle(result.Employee, result.Mode).Result;

                    if(result.Mode == CheckInOut.CheckIn)
                    {
                        serialPort.WriteLine("checkIn true " + result.Employee.FullName);
                        Console.WriteLine("Employee {0} checked in", result.Employee.FullName);

                        foreach (var device in devices)
                        {
                            _iotDeviceController.ControlDevice(device, true);
                            Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        serialPort.WriteLine("checkOut true " + result.Employee.FullName);
                        Console.WriteLine("Employee {0} checked out", result.Employee.FullName);

                        foreach (var device in devices)
                        {
                            _iotDeviceController.ControlDevice(device, false);
                            Thread.Sleep(1000);
                        }
                    }
                }
                else
                {
                    serialPort.WriteLine("checkIn false");
                    Console.WriteLine("Failed Check In/Check Out Attempt");
                }
            }
        }
    }
}

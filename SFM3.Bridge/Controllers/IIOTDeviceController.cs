﻿using SFM3.Shared.Models;

namespace SFM3.Bridge.Controllers
{
    internal interface IIOTDeviceController
    {
        void ControlDevice(Device device, bool state);
    }
}
﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Controllers
{
    internal class IOTDeviceController : IIOTDeviceController
    {
        private List<IOTDevice> devices = new List<IOTDevice>();

        public IOTDeviceController()
        {
            LoadDevices();
        }

        public void LoadDevices()
        {
            foreach (var device in ConfigurationManager.AppSettings.Get("DeviceControllers").Split(", "))
            {
                devices.Add(new IOTDevice(device));
            }
            Console.WriteLine("Loaded {0} Device Controllers", devices.Count);
        }

        public void ControlDevice(Device device, bool state)
        {
            devices.FirstOrDefault(d => d.Port == device.Port).ControlDevice(device.HardwareID, state);
        }
    }
}

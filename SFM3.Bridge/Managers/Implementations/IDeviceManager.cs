﻿using SFM3.Shared.Models;

namespace SFM3.Bridge.Managers.Implementations
{
    internal interface IDeviceManager
    {
        Task<IEnumerable<Device>> GetAllDevices();
    }
}
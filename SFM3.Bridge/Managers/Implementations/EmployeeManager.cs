﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Managers.Implementations
{
    internal class EmployeeManager : IEmployeeManager
    {
        private readonly HttpClient _httpClient;

        public EmployeeManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Employee>> GetAllEmployees()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Employee>>("/api/employees");
        }
    }
}

﻿using SFM3.Bridge.Managers.Implementations;
using SFM3.Bridge.Providers;
using SFM3.Client.Managers;
using SFM3.Shared.Models;
using SFM3.Shared.Models.AccessControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge
{
    internal class AccessManager : IAccessManager
    {
        private readonly IChipManager _chipManager;
        private readonly IEmployeeManager _employeeManager;
        private readonly IAreaProvider _areaProvider;
        private readonly HttpClient _httpClient;
        private Area currentArea;

        public AccessManager(IChipManager chipManager, IEmployeeManager employeeManager, IAreaProvider areaProvider, HttpClient httpClient)
        {
            _chipManager = chipManager;
            _employeeManager = employeeManager;
            _areaProvider = areaProvider;
            _httpClient = httpClient;
            currentArea = _areaProvider.GetContainingArea().Result;
        }

        public async Task<CheckInOutResult> CheckInOrOut(string chipCode)
        {
            var chip = (await _chipManager.GetAllChips()).FirstOrDefault(c => c.Code == chipCode);

            if (chip == null)
                return null;

            var checkInRequest = new CheckInRequest() { AreaId = currentArea.ID, EmployeeId = chip.Employee.ID };

            var response = await _httpClient.PostAsJsonAsync(chip.Employee.CheckedIn ? "api/access/checkOut" : "api/access/checkIn", checkInRequest);

            if (response.StatusCode == HttpStatusCode.OK)
                return new CheckInOutResult() { Employee = chip.Employee, Mode = chip.Employee.CheckedIn ? CheckInOut.CheckOut : CheckInOut.CheckIn };

            return null;
        }

        public async Task<IEnumerable<Device>> GetDevicesToToggle(Employee employee, CheckInOut inOut)
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Device>>("api/access/devicesOnCheck" + (inOut == CheckInOut.CheckIn ? "In" : "Out") + "/" + employee.ID);
        }
    }
}

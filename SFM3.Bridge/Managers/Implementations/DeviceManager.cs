﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Managers.Implementations
{
    internal class DeviceManager : IDeviceManager
    {
        private readonly HttpClient _httpClient;

        public DeviceManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Device>> GetAllDevices()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Device>>("api/Devices");
        }
    }
}

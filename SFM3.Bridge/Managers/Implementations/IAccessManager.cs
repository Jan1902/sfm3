﻿using SFM3.Shared.Models;
using SFM3.Shared.Models.AccessControl;

namespace SFM3.Bridge
{
    internal interface IAccessManager
    {
        Task<CheckInOutResult> CheckInOrOut(string chipCode);
        Task<IEnumerable<Device>> GetDevicesToToggle(Employee employee, CheckInOut inOut);
    }
}
﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Managers.Implementations
{
    internal class AreaManager : IAreaManager
    {
        private readonly HttpClient _httpClient;

        public AreaManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Area>> GetAllAreas()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Area>>("api/Areas");
        }
    }
}

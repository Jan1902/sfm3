﻿using SFM3.Shared.Models;

namespace SFM3.Bridge.Managers.Implementations
{
    internal interface IEmployeeManager
    {
        Task<IEnumerable<Employee>> GetAllEmployees();
    }
}
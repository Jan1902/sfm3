﻿using SFM3.Shared.Models;

namespace SFM3.Client.Managers
{
    public interface IChipManager
    {
        Task<IEnumerable<Chip>> GetAllChips();
    }
}
﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SFM3.Bridge.Controllers;
using SFM3.Bridge.Managers.Implementations;
using SFM3.Bridge.Providers;
using SFM3.Client.Managers;
using System.Configuration;

namespace SFM3.Bridge
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static void AddServices(IServiceCollection services)
        {
            var httpClient = new HttpClient() { BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("Url")) };
            httpClient.DefaultRequestHeaders.Add("ApiKey", ConfigurationManager.AppSettings.Get("ApiKey"));
            services.AddSingleton(httpClient);

            services.AddTransient<IAccessManager, AccessManager>();
            services.AddTransient<IAreaManager, AreaManager>();
            services.AddTransient<IChipManager, ChipManager>();
            services.AddTransient<IEmployeeManager, EmployeeManager>();

            services.AddTransient<IAreaProvider, TestAreaProvider>();

            services.AddSingleton<IIOTDeviceController, IOTDeviceController>();
            services.AddSingleton<AccessDeviceController>();
            services.BuildServiceProvider().GetService<AccessDeviceController>().LoadDevices();

            services.AddSingleton<IOTDeviceController>();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) => { AddServices(services); });
    }
}
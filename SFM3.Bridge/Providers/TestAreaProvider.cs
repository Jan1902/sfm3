﻿using SFM3.Bridge.Managers.Implementations;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Bridge.Providers
{
    internal class TestAreaProvider : IAreaProvider
    {
        private readonly IAreaManager _areaManager;

        public TestAreaProvider(IAreaManager areaManager)
        {
            _areaManager = areaManager;
        }

        public async Task<Area> GetContainingArea()
        {
            return (await _areaManager.GetAllAreas()).FirstOrDefault();
        }
    }
}

﻿using SFM3.Shared.Models;

namespace SFM3.Bridge.Providers
{
    internal interface IAreaProvider
    {
        Task<Area> GetContainingArea();
    }
}
﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SFM3.Server.Data;
using SFM3.Server.Repositories;
using SFM3.Shared;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SFM3.Server.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ChipsController : ControllerBase
    {
        private readonly IGenericRepository<Chip> _repository;
        
        public ChipsController(IGenericRepository<Chip> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _repository.GetAll(new string[] { "Employee" }));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (!(await _repository.GetAll(new string[] { "Employee" })).Any(e => e.ID == id))
                return BadRequest();

            return Ok(await _repository.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Add(Chip model)
        {
            var entry = await _repository.Insert(model);
            await _repository.Save();
            return Ok(entry);
        }

        [HttpPut]
        public async Task<IActionResult> Update(Chip model)
        {
            if (!(await _repository.GetAll()).Any(e => e.ID == model.ID))
                return BadRequest();

            await _repository.Update(model);
            await _repository.Save();
            return Ok();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!(await _repository.GetAll()).Any(e => e.ID == id))
                return BadRequest();

            var model = await _repository.GetById(id);
            await _repository.Delete(model);
            await _repository.Save();
            return Ok();
        }
    }
}

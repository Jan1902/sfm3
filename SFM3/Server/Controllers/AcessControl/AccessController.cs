﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SFM3.Server.Attributes;
using SFM3.Server.Repositories;
using SFM3.Shared.Models;
using SFM3.Shared.Models.AccessControl;
using System.Linq;
using System.Threading.Tasks;

namespace SFM3.Server.Controllers.AcessControl
{
    [ApiKey]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccessController : ControllerBase
    {
        private readonly IGenericRepository<Employee> _employeeRepository;
        private readonly IGenericRepository<Area> _areaRepository;

        public AccessController(IGenericRepository<Employee> employeeRepository, IGenericRepository<Area> areaRepository)
        {
            _employeeRepository = employeeRepository;
            _areaRepository = areaRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CheckIn(CheckInRequest checkIn)
        {
            var employee = await _employeeRepository.GetById(checkIn.EmployeeId, new string[] { "Accessable" });
            var area = await _areaRepository.GetById(checkIn.AreaId);

            if (employee == null || area == null)
                return NotFound();

            if(!employee.Accessable.Contains(area))
                return BadRequest();

            employee.CheckedIn = true;

            await _employeeRepository.Update(employee);
            await _employeeRepository.Save();

            return Ok(new CheckInOutResult() { Employee = employee });
        }

        [HttpPost]
        public async Task<IActionResult> CheckInOut(CheckInRequest checkIn)
        {
            var employee = await _employeeRepository.GetById(checkIn.EmployeeId, new string[] { "Accessable" });

            if (employee == null)
                return NotFound();

            if (!employee.CheckedIn)
            {
                var area = await _areaRepository.GetById(checkIn.AreaId);

                if (area == null)
                    return NotFound();

                if (!employee.Accessable.Contains(area))
                    return BadRequest();

            }

            employee.CheckedIn = !employee.CheckedIn;

            await _employeeRepository.Update(employee);
            await _employeeRepository.Save();

            return Ok(new CheckInOutResult() { Employee = employee, Mode = employee.CheckedIn ? Shared.Models.AccessControl.CheckInOut.CheckIn : Shared.Models.AccessControl.CheckInOut.CheckOut });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> DevicesOnCheckIn(int id)
        {
            var employee = await _employeeRepository.GetById(id, new string[] { "Office.Devices", "Office.Connected.Devices" });

            if (employee == null)
                return NotFound();

            return Ok(employee.Office.Devices
                        .Concat(employee.Office.Connected
                            .SelectMany(c => c.Devices)));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> DevicesOnCheckOut(int id)
        {
            var employee = await _employeeRepository.GetById(id, new string[] { "Office.Devices", "Office.Connected.Devices" });

            if (employee == null)
                return NotFound();

            return Ok(employee.Office.Devices
                    .Concat(employee.Office.Connected
                        .Where(o => _employeeRepository
                            .Find(e => e.CheckedIn == true
                                && e.Office.ID == o.ID) == null)
                        .SelectMany(c => c.Devices)));
        }

        [HttpPost]
        public async Task<IActionResult> CheckOut(CheckInRequest checkOut)
        {
            var employee = await _employeeRepository.GetById(checkOut.EmployeeId);
            var area = await _areaRepository.GetById(checkOut.AreaId);

            if (employee == null || area == null)
                return NotFound();

            employee.CheckedIn = false;

            await _employeeRepository.Update(employee);
            await _employeeRepository.Save();

            return Ok(new CheckInOutResult() { Employee = employee });
        }
    }
}

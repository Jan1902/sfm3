﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SFM3.Server.Data.Migrations
{
    public partial class addition7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RoomID",
                table: "Rooms",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Port",
                table: "Devices",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_RoomID",
                table: "Rooms",
                column: "RoomID");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_Rooms_RoomID",
                table: "Rooms",
                column: "RoomID",
                principalTable: "Rooms",
                principalColumn: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_Rooms_RoomID",
                table: "Rooms");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_RoomID",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "RoomID",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "Port",
                table: "Devices");
        }
    }
}

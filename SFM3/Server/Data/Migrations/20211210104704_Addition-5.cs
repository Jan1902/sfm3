﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SFM3.Server.Data.Migrations
{
    public partial class Addition5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Chips",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Chips");
        }
    }
}

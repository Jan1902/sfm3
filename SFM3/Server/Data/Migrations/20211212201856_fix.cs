﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SFM3.Server.Data.Migrations
{
    public partial class fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Areas_Areas_AreaID",
                table: "Areas");

            migrationBuilder.DropForeignKey(
                name: "FK_Areas_Employees_EmployeeID",
                table: "Areas");

            migrationBuilder.DropIndex(
                name: "IX_Areas_AreaID",
                table: "Areas");

            migrationBuilder.DropIndex(
                name: "IX_Areas_EmployeeID",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "AreaID",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "EmployeeID",
                table: "Areas");

            migrationBuilder.CreateTable(
                name: "AreaEmployee",
                columns: table => new
                {
                    AccessableByID = table.Column<int>(type: "int", nullable: false),
                    AccessableID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaEmployee", x => new { x.AccessableByID, x.AccessableID });
                    table.ForeignKey(
                        name: "FK_AreaEmployee_Areas_AccessableID",
                        column: x => x.AccessableID,
                        principalTable: "Areas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AreaEmployee_Employees_AccessableByID",
                        column: x => x.AccessableByID,
                        principalTable: "Employees",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AreaEmployee_AccessableID",
                table: "AreaEmployee",
                column: "AccessableID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AreaEmployee");

            migrationBuilder.AddColumn<int>(
                name: "AreaID",
                table: "Areas",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeID",
                table: "Areas",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Areas_AreaID",
                table: "Areas",
                column: "AreaID");

            migrationBuilder.CreateIndex(
                name: "IX_Areas_EmployeeID",
                table: "Areas",
                column: "EmployeeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Areas_Areas_AreaID",
                table: "Areas",
                column: "AreaID",
                principalTable: "Areas",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Areas_Employees_EmployeeID",
                table: "Areas",
                column: "EmployeeID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

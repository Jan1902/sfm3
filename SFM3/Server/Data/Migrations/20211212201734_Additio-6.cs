﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SFM3.Server.Data.Migrations
{
    public partial class Additio6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AreaID",
                table: "Areas",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Areas_AreaID",
                table: "Areas",
                column: "AreaID");

            migrationBuilder.AddForeignKey(
                name: "FK_Areas_Areas_AreaID",
                table: "Areas",
                column: "AreaID",
                principalTable: "Areas",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Areas_Areas_AreaID",
                table: "Areas");

            migrationBuilder.DropIndex(
                name: "IX_Areas_AreaID",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "AreaID",
                table: "Areas");
        }
    }
}

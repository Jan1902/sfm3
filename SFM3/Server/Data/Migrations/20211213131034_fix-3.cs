﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SFM3.Server.Data.Migrations
{
    public partial class fix3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AreaEmployee_Employees_AccessableByID",
                table: "AreaEmployee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee");

            migrationBuilder.DropIndex(
                name: "IX_AreaEmployee_AccessableID",
                table: "AreaEmployee");

            migrationBuilder.DropColumn(
                name: "UUID",
                table: "Chips");

            migrationBuilder.RenameColumn(
                name: "AccessableByID",
                table: "AreaEmployee",
                newName: "AcessableByID");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Chips",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee",
                columns: new[] { "AccessableID", "AcessableByID" });

            migrationBuilder.CreateIndex(
                name: "IX_AreaEmployee_AcessableByID",
                table: "AreaEmployee",
                column: "AcessableByID");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaEmployee_Employees_AcessableByID",
                table: "AreaEmployee",
                column: "AcessableByID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AreaEmployee_Employees_AcessableByID",
                table: "AreaEmployee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee");

            migrationBuilder.DropIndex(
                name: "IX_AreaEmployee_AcessableByID",
                table: "AreaEmployee");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Chips");

            migrationBuilder.RenameColumn(
                name: "AcessableByID",
                table: "AreaEmployee",
                newName: "AccessableByID");

            migrationBuilder.AddColumn<Guid>(
                name: "UUID",
                table: "Chips",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee",
                columns: new[] { "AccessableByID", "AccessableID" });

            migrationBuilder.CreateIndex(
                name: "IX_AreaEmployee_AccessableID",
                table: "AreaEmployee",
                column: "AccessableID");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaEmployee_Employees_AccessableByID",
                table: "AreaEmployee",
                column: "AccessableByID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

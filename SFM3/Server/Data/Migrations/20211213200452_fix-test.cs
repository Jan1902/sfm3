﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SFM3.Server.Data.Migrations
{
    public partial class fixtest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Devices_Rooms_RoomID",
                table: "Devices");

            migrationBuilder.DropIndex(
                name: "IX_Devices_RoomID",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "RoomID",
                table: "Devices");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RoomID",
                table: "Devices",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Devices_RoomID",
                table: "Devices",
                column: "RoomID");

            migrationBuilder.AddForeignKey(
                name: "FK_Devices_Rooms_RoomID",
                table: "Devices",
                column: "RoomID",
                principalTable: "Rooms",
                principalColumn: "ID");
        }
    }
}

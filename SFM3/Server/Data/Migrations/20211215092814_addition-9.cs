﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SFM3.Server.Data.Migrations
{
    public partial class addition9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AreaEmployee_Employees_AcessableByID",
                table: "AreaEmployee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee");

            migrationBuilder.DropIndex(
                name: "IX_AreaEmployee_AcessableByID",
                table: "AreaEmployee");

            migrationBuilder.RenameColumn(
                name: "AcessableByID",
                table: "AreaEmployee",
                newName: "AccessableByID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee",
                columns: new[] { "AccessableByID", "AccessableID" });

            migrationBuilder.CreateIndex(
                name: "IX_AreaEmployee_AccessableID",
                table: "AreaEmployee",
                column: "AccessableID");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaEmployee_Employees_AccessableByID",
                table: "AreaEmployee",
                column: "AccessableByID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AreaEmployee_Employees_AccessableByID",
                table: "AreaEmployee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee");

            migrationBuilder.DropIndex(
                name: "IX_AreaEmployee_AccessableID",
                table: "AreaEmployee");

            migrationBuilder.RenameColumn(
                name: "AccessableByID",
                table: "AreaEmployee",
                newName: "AcessableByID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaEmployee",
                table: "AreaEmployee",
                columns: new[] { "AccessableID", "AcessableByID" });

            migrationBuilder.CreateIndex(
                name: "IX_AreaEmployee_AcessableByID",
                table: "AreaEmployee",
                column: "AcessableByID");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaEmployee_Employees_AcessableByID",
                table: "AreaEmployee",
                column: "AcessableByID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

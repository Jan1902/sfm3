﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SFM3.Server.Data.Migrations
{
    public partial class addition8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CheckedIn",
                table: "Employees",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CheckedIn",
                table: "Employees");
        }
    }
}

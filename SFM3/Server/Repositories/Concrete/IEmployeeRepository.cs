﻿using SFM3.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFM3.Server.Repositories
{
    public interface IEmployeeRepository
    {
        Task CreateEmployee(Employee employee);
        Task DeleteEmployee(Employee employee);
        Task<IEnumerable<Employee>> GetAllEmployees();
        Task UpdateEmployee(Employee employee);
    }
}
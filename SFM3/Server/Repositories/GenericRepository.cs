﻿using Microsoft.EntityFrameworkCore;
using SFM3.Server.Data;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SFM3.Server.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : Entity
    {
        private readonly ApplicationDbContext _context;

        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _context.Set<T>().AsNoTrackingWithIdentityResolution().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAll(string[] includes)
        {
            var query = _context.Set<T>().Include(includes[0]);

            foreach (var include in includes.Skip(1))
            {
                query = query.Include(include);
            }

            return await query.AsNoTrackingWithIdentityResolution().ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task<T> GetById(int id, string[] includes)
        {
            var query = _context.Set<T>().Include(includes[0]);

            foreach (var include in includes.Skip(1))
            {
                query = query.Include(include);
            }

            return await query.FirstOrDefaultAsync(i => i.ID == id);
        }

        public async Task<T> Insert(T obj)
        {
            await _context.Set<T>().AddAsync(obj);
            return obj;
        }

        public async Task Update(T obj)
        {
            //_context.Entry(obj).State = EntityState.Modified;
            _context.Set<T>().Update(obj);
        }

        public async Task Delete(T obj)
        {
            _context.Set<T>().Remove(obj);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> expression, string[] includes)
        {
            var query = _context.Set<T>().Where(expression);

            foreach (var include in includes.Skip(1))
            {
                query = query.Include(include);
            }

            return await query.ToListAsync();
        }
    }
}

﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SFM3.Server.Repositories
{
    public interface IGenericRepository<T> where T : Entity
    {
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetAll(string[] includes);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> expression);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> expression, string[] includes);
        Task<T> GetById(int id);
        Task<T> GetById(int id, string[] includes);
        Task<T> Insert(T obj);
        Task Update(T obj);
        Task Delete(T obj);
        Task Save();
    }
}

﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models.AccessControl
{
    public class CheckInOutResult
    {
        public Employee Employee { get; set; }
        public CheckInOut Mode { get; set; }
    }

    public enum CheckInOut
    {
        CheckIn,
        CheckOut
    }
}

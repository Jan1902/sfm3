﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models.AccessControl
{
    public class CheckInRequest
    {
        public int EmployeeId { get; set; }
        public int AreaId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models
{
    public class Chip : Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual Employee Employee { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models
{
    public class Employee : Entity
    {
        public string Short { get; set; }
        public string FullName { get; set; }
        public virtual Room Office { get; set; }
        public virtual IEnumerable<Area> Accessable { get; set; }
        public bool CheckedIn { get; set; }

        public Employee()
        {
            Accessable = new List<Area>();
        }
    }
}

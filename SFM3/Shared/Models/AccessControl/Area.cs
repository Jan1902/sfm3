﻿using SFM3.Shared.Models.AccessControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models
{
    public class Area : Entity
    {
        public string Name { get; set; }
        public virtual IEnumerable<Employee> AccessableBy { get; set; }

        public Area()
        {
            AccessableBy = new List<Employee>();
        }
    }
}

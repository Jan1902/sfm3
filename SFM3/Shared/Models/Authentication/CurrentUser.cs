﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models.Authentication
{
    public class CurrentUser
    {
        public bool IsAuthenticated { get; set; }
        public string Username { get; set; }
        public Dictionary<string, string> Claims { get; set; }
    }
}

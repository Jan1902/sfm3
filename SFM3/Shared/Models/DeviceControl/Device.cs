﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models
{
    public class Device : Entity
    {
        public DeviceType Type { get; set; }
        public string Name { get; set; }
        public bool State { get; set; }
        public int HardwareID { get; set; }
        public string Port { get; set; }
    }

    public enum DeviceType : int
    {
        OnOff = 0
    }
}

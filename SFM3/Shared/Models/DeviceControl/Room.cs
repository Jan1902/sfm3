﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFM3.Shared.Models
{
    public class Room : Entity
    {
        public string Name { get; set; }
        public IEnumerable<Device> Devices { get; set; }
        public IEnumerable<Room> Connected { get; set; }
    }
}

﻿using SFM3.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFM3.Client.Managers.Implementations
{
    public interface IGenericManager<T> where T : Entity
    {
        Task Create(T obj);
        Task Delete(int id);
        Task<IEnumerable<T>> GetAll();
        Task Update(T obj);
    }
}
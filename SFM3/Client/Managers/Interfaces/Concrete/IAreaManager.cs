﻿using SFM3.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public interface IAreaManager
    {
        Task CreateArea(Area Area);
        Task DeleteArea(int id);
        Task<IEnumerable<Area>> GetAllAreas();
        Task UpdateArea(Area Area);
    }
}
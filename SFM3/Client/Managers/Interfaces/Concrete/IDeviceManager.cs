﻿using SFM3.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public interface IDeviceManager
    {
        Task CreateDevice(Device Device);
        Task DeleteDevice(int id);
        Task<IEnumerable<Device>> GetAllDevices();
        Task UpdateDevice(Device Device);
    }
}
﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    interface IRoomManager
    {
        Task<IEnumerable<Room>> GetAllRooms();
        Task DeleteRoom(int id);
        Task UpdateRoom(Room Room);
        Task CreateRoom(Room Room);
    }
}

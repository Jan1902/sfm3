﻿using SFM3.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public interface IChipManager
    {
        Task CreateChip(Chip Chip);
        Task DeleteChip(int id);
        Task<IEnumerable<Chip>> GetAllChips();
        Task UpdateChip(Chip Chip);
    }
}
﻿using SFM3.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public interface IEmployeeManager
    {
        Task<IEnumerable<Employee>> GetAllEmployees();
        Task DeleteEmployee(int id);
        Task UpdateEmployee(Employee employee);
        Task CreateEmployee(Employee employee);
    }
}
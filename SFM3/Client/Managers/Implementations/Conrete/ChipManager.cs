﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public class ChipManager : IChipManager
    {
        private readonly HttpClient _httpClient;

        public ChipManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Chip>> GetAllChips()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Chip>>("api/Chips");
        }

        public async Task DeleteChip(int id)
        {
            await _httpClient.DeleteAsync("api/Chips/" + id);
        }

        public async Task UpdateChip(Chip Chip)
        {
            await _httpClient.PutAsJsonAsync("api/Chips", Chip);
        }

        public async Task CreateChip(Chip Chip)
        {
            await _httpClient.PostAsJsonAsync("api/Chips", Chip);
        }
    }
}

﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public class EmployeeManager : IEmployeeManager
    {
        private readonly HttpClient _httpClient;

        public EmployeeManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Employee>> GetAllEmployees()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Employee>>("api/employees");
        }

        public async Task DeleteEmployee(int id)
        {
            await _httpClient.DeleteAsync("api/employees/" + id);
        }

        public async Task UpdateEmployee(Employee employee)
        {
            await _httpClient.PutAsJsonAsync("api/employees", employee);
        }

        public async Task CreateEmployee(Employee employee)
        {
            await _httpClient.PostAsJsonAsync("api/employees", employee);
        }
    }
}

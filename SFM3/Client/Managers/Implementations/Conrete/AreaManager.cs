﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public class AreaManager : IAreaManager
    {
        private readonly HttpClient _httpClient;

        public AreaManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Area>> GetAllAreas()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Area>>("api/Areas");
        }

        public async Task DeleteArea(int id)
        {
            await _httpClient.DeleteAsync("api/Areas/" + id);
        }

        public async Task UpdateArea(Area Area)
        {
            await _httpClient.PutAsJsonAsync("api/Areas", Area);
        }

        public async Task CreateArea(Area Area)
        {
            await _httpClient.PostAsJsonAsync("api/Areas", Area);
        }
    }
}

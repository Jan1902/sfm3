﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public class DeviceManager : IDeviceManager
    {
        private readonly HttpClient _httpClient;

        public DeviceManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Device>> GetAllDevices()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Device>>("api/Devices");
        }

        public async Task DeleteDevice(int id)
        {
            await _httpClient.DeleteAsync("api/Devices/" + id);
        }

        public async Task UpdateDevice(Device Device)
        {
            await _httpClient.PutAsJsonAsync("api/Devices", Device);
        }

        public async Task CreateDevice(Device Device)
        {
            await _httpClient.PostAsJsonAsync("api/Devices", Device);
        }
    }
}

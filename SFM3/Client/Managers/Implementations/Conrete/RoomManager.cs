﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public class RoomManager : IRoomManager
    {
        private readonly HttpClient _httpClient;

        public RoomManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Room>> GetAllRooms()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<Room>>("api/Rooms");
        }

        public async Task DeleteRoom(int id)
        {
            await _httpClient.DeleteAsync("api/Rooms/" + id);
        }

        public async Task UpdateRoom(Room Room)
        {
            await _httpClient.PutAsJsonAsync("api/Rooms", Room);
        }

        public async Task CreateRoom(Room Room)
        {
            await _httpClient.PostAsJsonAsync("api/Rooms", Room);
        }
    }
}

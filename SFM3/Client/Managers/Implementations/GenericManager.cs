﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SFM3.Client.Managers.Implementations
{
    public class GenericManager<T> : IGenericManager<T> where T : Entity
    {
        private readonly HttpClient _httpClient;

        public GenericManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<T>>("api/" + typeof(T).Name + "s");
        }

        public async Task Delete(int id)
        {
            await _httpClient.DeleteAsync("api/" + typeof(T).Name + "s" + "/" + id);
        }

        public async Task Update(T obj)
        {
            await _httpClient.PutAsJsonAsync("api/" + typeof(T).Name + "s", obj);
        }

        public async Task Create(T obj)
        {
            await _httpClient.PostAsJsonAsync("api/" + typeof(T).Name + "s", obj);
        }
    }
}

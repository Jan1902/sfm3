﻿using SFM3.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SFM3.Client.Managers
{
    public class MockRoomManager : IRoomManager
    {
        public Task CreateRoom(Room Room)
        {
            throw new NotImplementedException();
        }

        public Task DeleteRoom(int id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateRoom(Room Room)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Room>> GetAllRooms()
        {
            return new List<Room>() { new Room() { Name = "101" }, new Room() { Name = "202" } };
        }
    }
}

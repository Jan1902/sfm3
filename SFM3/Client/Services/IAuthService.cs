﻿using SFM3.Shared.Models.Authentication;
using System.Threading.Tasks;

namespace SFM3.Client.Services
{
    public interface IAuthService
    {
        Task<CurrentUser> CurrentUserInfo();
        Task Login(LoginRequest loginRequest);
        Task Logout();
        Task Register(RegisterRequest registerRequest);
    }
}
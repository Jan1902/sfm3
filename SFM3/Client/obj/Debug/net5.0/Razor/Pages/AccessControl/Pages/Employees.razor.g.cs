#pragma checksum "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fa034db1318d924e95c8a218b3d92ffa0f457803"
// <auto-generated/>
#pragma warning disable 1591
namespace SFM3.Client.Pages.AccessControl.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using SFM3.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using SFM3.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using MudBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\_Imports.razor"
using MudBlazor.ThemeManager;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
using SFM3.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
using System.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
using SFM3.Client.Managers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
using SFM3.Client.Managers.Implementations;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
using SFM3.Client.Pages.AccessControl.Modals;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/employees")]
    public partial class Employees : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<MudBlazor.MudContainer>(0);
            __builder.AddAttribute(1, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __Blazor.SFM3.Client.Pages.AccessControl.Pages.Employees.TypeInference.CreateMudTable_0(__builder2, 2, 3, "margin-top: 1rem", 4, 
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                               employees

#line default
#line hidden
#nullable disable
                , 5, 
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                 true

#line default
#line hidden
#nullable disable
                , 6, 
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                loading

#line default
#line hidden
#nullable disable
                , 7, 
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                                 new Func<Employee,bool>(FilterFunc)

#line default
#line hidden
#nullable disable
                , 8, 
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                                                                                   true

#line default
#line hidden
#nullable disable
                , 9, 
#nullable restore
#line 12 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                                                                                                      true

#line default
#line hidden
#nullable disable
                , 10, (__builder3) => {
                    __builder3.OpenComponent<MudBlazor.MudText>(11);
                    __builder3.AddAttribute(12, "Typo", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Typo>(
#nullable restore
#line 14 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                           Typo.h6

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(13, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(14, "Mitarbeiter");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(15, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudSpacer>(16);
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(17, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTooltip>(18);
                    __builder3.AddAttribute(19, "Text", "Neu");
                    __builder3.AddAttribute(20, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.OpenComponent<MudBlazor.MudIconButton>(21);
                        __builder4.AddAttribute(22, "Icon", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 17 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                      Icons.Material.Outlined.Add

#line default
#line hidden
#nullable disable
                        ));
                        __builder4.AddAttribute(23, "OnClick", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 17 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                              () => InvokeModal(null)

#line default
#line hidden
#nullable disable
                        )));
                        __builder4.CloseComponent();
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(24, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTooltip>(25);
                    __builder3.AddAttribute(26, "Text", "Reload");
                    __builder3.AddAttribute(27, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.OpenComponent<MudBlazor.MudIconButton>(28);
                        __builder4.AddAttribute(29, "Icon", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 20 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                      Icons.Material.Outlined.Refresh

#line default
#line hidden
#nullable disable
                        ));
                        __builder4.AddAttribute(30, "OnClick", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 20 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                LoadElements

#line default
#line hidden
#nullable disable
                        )));
                        __builder4.CloseComponent();
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(31, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudSpacer>(32);
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(33, "\r\n            ");
                    __Blazor.SFM3.Client.Pages.AccessControl.Pages.Employees.TypeInference.CreateMudTextField_1(__builder3, 34, 35, "Search", 36, 
#nullable restore
#line 23 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                     Adornment.Start

#line default
#line hidden
#nullable disable
                    , 37, 
#nullable restore
#line 23 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                                                      Icons.Material.Filled.Search

#line default
#line hidden
#nullable disable
                    , 38, 
#nullable restore
#line 23 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                                                                                              Size.Medium

#line default
#line hidden
#nullable disable
                    , 39, "mt-0", 40, 
#nullable restore
#line 23 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                       searchString

#line default
#line hidden
#nullable disable
                    , 41, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => searchString = __value, searchString)));
                }
                , 42, (__builder3) => {
                    __builder3.OpenComponent<MudBlazor.MudTh>(43);
                    __builder3.AddAttribute(44, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(45, "ID");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(46, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTh>(47);
                    __builder3.AddAttribute(48, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddMarkupContent(49, "Kürzel");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(50, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTh>(51);
                    __builder3.AddAttribute(52, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(53, "Name");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(54, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTh>(55);
                    __builder3.AddAttribute(56, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddMarkupContent(57, "Büro");
                    }
                    ));
                    __builder3.CloseComponent();
                }
                , 58, (context) => (__builder3) => {
                    __builder3.OpenComponent<MudBlazor.MudTd>(59);
                    __builder3.AddAttribute(60, "DataLabel", "Nr");
                    __builder3.AddAttribute(61, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 32 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
__builder4.AddContent(62, context.ID);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(63, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTd>(64);
                    __builder3.AddAttribute(65, "DataLabel", "Sign");
                    __builder3.AddAttribute(66, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 33 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
__builder4.AddContent(67, context.Short);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(68, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTd>(69);
                    __builder3.AddAttribute(70, "DataLabel", "Name");
                    __builder3.AddAttribute(71, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 34 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
__builder4.AddContent(72, context.FullName);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(73, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTd>(74);
                    __builder3.AddAttribute(75, "DataLabel", "Office");
                    __builder3.AddAttribute(76, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 35 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
__builder4.AddContent(77, context.Office?.Name);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(78, "\r\n            ");
                    __builder3.OpenComponent<MudBlazor.MudTd>(79);
                    __builder3.AddAttribute(80, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.OpenComponent<MudBlazor.MudTooltip>(81);
                        __builder4.AddAttribute(82, "Text", "Bearbeiten");
                        __builder4.AddAttribute(83, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.OpenComponent<MudBlazor.MudIconButton>(84);
                            __builder5.AddAttribute(85, "Icon", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 38 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                          Icons.Material.Filled.Edit

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(86, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 38 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                () => InvokeModal(context)

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.CloseComponent();
                        }
                        ));
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(87, "\r\n                ");
                        __builder4.OpenComponent<MudBlazor.MudTooltip>(88);
                        __builder4.AddAttribute(89, "Text", "Löschen");
                        __builder4.AddAttribute(90, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.OpenComponent<MudBlazor.MudIconButton>(91);
                            __builder5.AddAttribute(92, "Icon", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 41 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                          Icons.Material.Filled.Delete

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.AddAttribute(93, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 41 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
                                                                                  () => DeleteElement(context)

#line default
#line hidden
#nullable disable
                            ));
                            __builder5.CloseComponent();
                        }
                        ));
                        __builder4.CloseComponent();
                    }
                    ));
                    __builder3.CloseComponent();
                }
                , 94, (__builder3) => {
                    __builder3.OpenComponent<MudBlazor.MudTablePager>(95);
                    __builder3.CloseComponent();
                }
                );
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 51 "C:\Users\there\Source\Repos\sfm3\SFM3\Client\Pages\AccessControl\Pages\Employees.razor"
       
    private List<Employee> employees = new List<Employee>();
    private bool loading;
    private string searchString = "";

    protected override async Task OnInitializedAsync()
    {
        try
        {
            await LoadElements();
        }
        catch (Exception e)
        {
            await DialogService.ShowMessageBox("Error", "Es ist ein Fehler aufgetreten");
            await DialogService.ShowMessageBox("Error", e.ToString());
        }
    }

    private bool FilterFunc(Employee element)
    {
        if (string.IsNullOrWhiteSpace(searchString))
            return true;

        foreach (var prop in element.GetType().GetProperties())
        {
            if (prop.GetValue(element)?.ToString().Contains(searchString, StringComparison.OrdinalIgnoreCase) ?? false)
                return true;
        }

        return false;
    }

    private async Task LoadElements()
    {
        loading = true;
        StateHasChanged();
        employees = (await EmployeeManager.GetAll()).ToList();
        StateHasChanged();
        loading = false;
    }

    private async Task DeleteElement(Employee element)
    {
        if (await DialogService.ShowMessageBox("Löschen?", "Möchten Sie diesen Mitarbeiter wirklich löschen?", yesText: "Ja", cancelText: "Abbrechen") == true)
        {
            employees.Remove(element);
            await EmployeeManager.Delete(element.ID);
        }
    }

    private async Task InvokeModal(Employee element)
    {
        var parameters = new DialogParameters();

        if (element != null)
            parameters.Add(nameof(AddEditEmployeesModal.Element), element);

        var dialog = DialogService.Show<AddEditEmployeesModal>("Mitarbeiter hinzufügen", parameters);
        if (!(await dialog.Result).Cancelled)
        {
            await LoadElements();
            StateHasChanged();
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient HttpClient { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IGenericManager<Employee> EmployeeManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IDialogService DialogService { get; set; }
    }
}
namespace __Blazor.SFM3.Client.Pages.AccessControl.Pages.Employees
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateMudTable_0<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.Collections.Generic.IEnumerable<T> __arg1, int __seq2, global::System.Boolean __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Func<T, global::System.Boolean> __arg4, int __seq5, global::System.Boolean __arg5, int __seq6, global::System.Boolean __arg6, int __seq7, global::Microsoft.AspNetCore.Components.RenderFragment __arg7, int __seq8, global::Microsoft.AspNetCore.Components.RenderFragment __arg8, int __seq9, global::Microsoft.AspNetCore.Components.RenderFragment<T> __arg9, int __seq10, global::Microsoft.AspNetCore.Components.RenderFragment __arg10)
        {
        __builder.OpenComponent<global::MudBlazor.MudTable<T>>(seq);
        __builder.AddAttribute(__seq0, "Style", __arg0);
        __builder.AddAttribute(__seq1, "Items", __arg1);
        __builder.AddAttribute(__seq2, "Hover", __arg2);
        __builder.AddAttribute(__seq3, "Loading", __arg3);
        __builder.AddAttribute(__seq4, "Filter", __arg4);
        __builder.AddAttribute(__seq5, "FixedFooter", __arg5);
        __builder.AddAttribute(__seq6, "FixedHeader", __arg6);
        __builder.AddAttribute(__seq7, "ToolBarContent", __arg7);
        __builder.AddAttribute(__seq8, "HeaderContent", __arg8);
        __builder.AddAttribute(__seq9, "RowTemplate", __arg9);
        __builder.AddAttribute(__seq10, "PagerContent", __arg10);
        __builder.CloseComponent();
        }
        public static void CreateMudTextField_1<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::MudBlazor.Adornment __arg1, int __seq2, global::System.String __arg2, int __seq3, global::MudBlazor.Size __arg3, int __seq4, global::System.String __arg4, int __seq5, T __arg5, int __seq6, global::Microsoft.AspNetCore.Components.EventCallback<T> __arg6)
        {
        __builder.OpenComponent<global::MudBlazor.MudTextField<T>>(seq);
        __builder.AddAttribute(__seq0, "Placeholder", __arg0);
        __builder.AddAttribute(__seq1, "Adornment", __arg1);
        __builder.AddAttribute(__seq2, "AdornmentIcon", __arg2);
        __builder.AddAttribute(__seq3, "IconSize", __arg3);
        __builder.AddAttribute(__seq4, "Class", __arg4);
        __builder.AddAttribute(__seq5, "Value", __arg5);
        __builder.AddAttribute(__seq6, "ValueChanged", __arg6);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
